/* global $pdk */
var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.ThePlatform = youbora.Adapter.extend({

  constructor: function (player, options) {
    if (options && options.accountCode) {
      this.options = options
      this.scope = this.options.scope
    } else {
      this.scope = options
    }
    youbora.adapters.ThePlatform.__super__.constructor.call(this, player)
  },

  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.playhead
  },

  /** Override to return video duration */
  getDuration: function () {
    return this.duration
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    return this.bitrate
  },

  /** Override to return rendition */
  getRendition: function () {
    var ret = null
    if (this.bitrate > 0) {
      ret = youbora.Util.buildRenditionString(this.width, this.height, this.bitrate)
    }
    return ret
  },

  /** Override to return title */
  getTitle: function () {
    return this.title
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    return this.isLive
  },

  /** Override to return resource URL. */
  getResource: function () {
    return this.resource
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    return $pdk ? $pdk.version.toString() : ''
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'ThePlatform'
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Console all events if logLevel=DEBUG
    youbora.Util.logAllEvents(this.player, [null,
      'OnMediaPlaying',
      'OnMediaPause',
      'OnMediaUnpause',
      'OnClipInfoLoaded',
      'OnMediaBuffering',
      'OnMediaEnd',
      'OnMediaError',
      'OnVersionError',
      'OnMediaPlay',
      'OnMediaSeek',
      'OnMediaStart',
      'OnMediaLoadStart',
      'OnResetPlayer',
      'OnReleaseEnd',
      'OnReleaseStart',
      'OnReleaseSelected'
    ])

    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, false)

    // Register listeners
    this.references = {
      OnReleaseStart: this.releaseStartListener.bind(this),
      OnMediaLoadStart: this.mediaLoadListener.bind(this),
      OnRenditionSwitched: this.renditionListener.bind(this),
      OnMediaPlaying: this.playingListener.bind(this),
      OnMediaPause: this.pauseListener.bind(this),
      OnMediaUnpause: this.unpauseListener.bind(this),
      OnReleaseEnd: this.releaseEndListener.bind(this),
      OnMediaError: this.mediaErrorListener.bind(this),
      OnMediaSeek: this.mediaSeekListener.bind(this),
      OnMediaStart: this.mediaStartListener.bind(this),
      OnReleaseSelected: this.releaseSelected.bind(this)
    }
    for (var key in this.references) {
      this.player.addEventListener(key, this.references[key], this.scope)
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
      }
      delete this.references
    }
  },

  releaseSelected: function (e) {
    this._includeAdsAdapter()
  },

  /** Listener for 'OnReleaseStart' event. */
  releaseStartListener: function (e) {
    this._includeAdsAdapter()
    if (e.data.baseClips) {
      var clip
      for (var i in e.data.baseClips) {
        clip = e.data.baseClips[i]
        if (!clip.isAd) {
          this.resource = clip.URL
          this.title = clip.title
          if (!this.title) {
            if (e.data.title) this.title = e.data.title
          }
          this.duration = clip.trueLength / 1000
          this.isLive = (clip.expression === 'nonstop')
          this.bitrate = clip.bitrate || 0
          if (this.bitrate > 6e7) this.bitrate /= 1000
          break
        }
      }
    }
    for (var j = 0; j < 20; j++) {
      if (this.plugin.options['extraparam.' + (j + 1)] === '{tp_aid}') {
        this.plugin.options['extraparam.' + (j + 1)] = e.data.accountID
        break
      }
      if (this.plugin.options['content.customDimension.' + (j + 1)] === '{tp_aid}') {
        this.plugin.options['content.customDimension.' + (j + 1)] = e.data.accountID
        break
      }
    }
    this.fireStart()
    this.stoppedByError = false
  },

  /** Listener for 'OnMediaLoadStart' event. */
  mediaLoadListener: function (e) {
    this._includeAdsAdapter()
    this.plugin.getAdsAdapter().loadListener(e)
    if (e.data.baseClip.contentCustomData && e.data.baseClip.contentCustomData.isException && this.stoppedByError) {
      this.stoppedByError = true
      this.fireError(e.data.baseClip.contentCustomData.responseCode, e.data.baseClip.contentCustomData.exception)
    } else if (!e.data.baseClip.isAd) {
      this.duration = e.data.baseClip.trueLength / 1000
      this.bitrate = e.data.baseClip.bitrate || 0
      if (this.bitrate > 6e7) this.bitrate /= 1000
    } else {
      if (this.plugin.getAdsAdapter() && this.plugin.getAdsAdapter().loadListener) {
        this.plugin.getAdsAdapter().loadListener(e)
      }
    }
  },

  /** Listener for 'OnRenditionSwitched' event. */
  renditionListener: function (e) {
    if (e.newRendition) {
      this.bitrate = e.newRendition
    } else if (e.data && e.data.newRendition && e.data.newRendition.bitrate) {
      this.bitrate = e.data.newRendition.bitrate
      this.width = e.data.newRendition.width
      this.height = e.data.newRendition.height
    }
  },

  /** Listener for 'OnMediaPlaying' event. */
  playingListener: function (e) {
    if (!this.plugin.getAdsAdapter() || (this.plugin.getAdsAdapter() &&
      !this.plugin.getAdsAdapter().flags.isStarted)) {
      this.playhead = (e.data.currentTimeAggregate || e.data.currentTime)
      if (!this.plugin.getIsLive()) {
        if (this.playhead > this.plugin.getDuration() * e.data.percentComplete / 99) {
          this.playhead /= 1e3
        }
      }
      if (!this.flags.isBuffering) {
        if (!this.flags.isJoined) {
          this.fireJoin()
        } else if (this.flags.isSeeking && this.playhead > this.seekingTo) {
          this.fireSeekEnd()
        }
      }
    }
  },

  /** Listener for 'OnMediaPause' event. */
  pauseListener: function (e) {
    if (!this.plugin.getAdsAdapter() || (this.plugin.getAdsAdapter() && !this.plugin.getAdsAdapter().flags.isStarted)) {
      this.firePause()
    }
  },

  /** Listener for 'OnMediaUnpause' event. */
  unpauseListener: function (e) {
    if (!this.plugin.getAdsAdapter() || (this.plugin.getAdsAdapter() && !this.plugin.getAdsAdapter().flags.isStarted)) {
      this.fireResume()
    }
  },

  /** Listener for 'OnReleaseEnd' event. */
  releaseEndListener: function (e) {
    this.fireStop() // should be called when going to background with phones
    this.resetValues()
  },

  /** Listener for 'OnMediaError' event. */
  mediaErrorListener: function (e) {
    if (!e.data.clip.baseClip.isAd && !this.stoppedByError) {
      this.stoppedByError = true
      this.resource = e.data.clip.URL || e.data.baseClip.URL
      this.title = e.data.clip.title
      this.duration = e.data.clip.mediaLength / 1000
      this.fireError(e.data.friendlyMessage || e.data.message)
      this.fireStop()
    } else {
      this.fireJoin()
    }
  },

  /** Listener for 'OnMediaSeek' event. */
  mediaSeekListener: function (e) {
    this.fireSeekBegin()
    this.seekingTo = (e.data.end.currentTimeAggregate ? e.data.end.currentTimeAggregate : e.data.end.currentTime) / 1000
  },

  mediaStartListener: function (e) {
    // ...
  },

  resetValues: function () {
    this.resource = null
    this.title = null
    this.duration = null
    this.seekingTo = null
    this.playhead = null
    this.bitrate = null
    this.isLive = null
  },

  _includeAdsAdapter: function () {
    if (!this.plugin.getAdsAdapter()) {
      this.plugin.setAdsAdapter(new youbora.adapters.ThePlatform.AdsAdapter(this.player, this.scope))
    }
  }
}, {
  AdsAdapter: require('./ads/ads')
})

module.exports = youbora.adapters.ThePlatform
