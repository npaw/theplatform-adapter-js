var youbora = require('youboralib')
require('./adapter')
if (typeof $pdk !== 'undefined' && $pdk.controller.plugInLoaded) {
  require('./register-plugin')
}
module.exports = youbora
