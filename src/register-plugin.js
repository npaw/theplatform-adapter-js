/* global $pdk, tpDebug, youboraAnalytics */
var youbora = require('youboralib')

if ($pdk && $pdk.controller) {
  $pdk.ns('$pdk.plugin.youbora')
  $pdk.plugin.youbora = $pdk.extend(function () { }, {
    VERSION: '1.0',

    constructor: function () {
      tpDebug('Youbora Plugin instantiated.')
    },

    /**
     * Initialize the plugin with the load object
     * @param lo load object
     */
    initialize: function (lo) {
      if (lo.vars.accountCode) {
        var options = { accountCode: lo.vars.accountCode, extraParams: {} }
        if (lo.vars.extraparams) {
          var paramlist = lo.vars.extraparams.split(',')
          options.extraParams = []
          for (var i = 0; i < paramlist.length; i++) {
            options.extraParams.push(paramlist[i])
          }
        }
        if (lo.vars.scope) {
          options.scope = lo.vars.scope
        }
        window.youboraAnalytics = new youbora.Plugin({ accountCode: lo.vars.accountCode })
        youboraAnalytics.options.setCustomDimensions(options.extraParams)
        youboraAnalytics.setAdapter(new youbora.adapters.ThePlatform(lo.controller, options))
      }
    }
  })
  $pdk.controller.plugInLoaded(new $pdk.plugin.youbora(), null)
}
