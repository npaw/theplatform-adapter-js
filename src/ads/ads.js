/* global $pdk */
var youbora = require('youboralib')
var manifest = require('../../manifest.json')

var AdsAdapter = youbora.Adapter.extend({

  constructor: function (player, scope) {
    this.scope = scope
    youbora.adapters.ThePlatform.AdsAdapter.__super__.constructor.call(this, player)
  },

  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.playhead || 0
  },

  /** Override to return video duration */
  getDuration: function () {
    return this.duration
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    return this.bitrate
  },

  /** Override to return title */
  getTitle: function () {
    return this.title
  },

  /** Override to return resource URL. */
  getResource: function () {
    return this.resource
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    return $pdk ? $pdk.version.toString() : ''
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'ThePlatform'
  },

  /** Override to return current ad position (only ads) */
  getPosition: function () {
    return this.adPosition
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, false)

    // Register listeners
    this.references = {
      'OnMediaLoadStart': this.loadListener.bind(this),
      'OnMediaError': this.errorListener.bind(this),
      'OnMediaEnd': this.endListener.bind(this),
      'OnMediaPlaying': this.playingListener.bind(this),
      'OnMediaPause': this.pauseListener.bind(this),
      'OnMediaUnpause': this.unpauseListener.bind(this),
      'OnMediaClick': this.clickListener.bind(this)
    }
    for (var key in this.references) {
      this.player.addEventListener(key, this.references[key], this.scope)
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
      }
      delete this.references
    }
  },

  /** Listener for 'OnMediaLoadStart' event. */
  loadListener: function (e) {
    try {
      if (e.data.baseClip.isAd) {
        this.duration = e.data.baseClip.releaseLength / 1000
        this.bitrate = e.data.baseClip.bitrate || 0
        this.resource = e.data.baseClip.URL
        this.title = e.data.title
        if (e.data.baseClip.moreInfo) this.clickURL = e.data.baseClip.moreInfo.href
        this.adPosition = 'pre'
        if (this.plugin.getAdapter().getDuration() <= this.plugin.getAdapter().getPlayhead() + 2) {
          this.adPosition = 'post'
        } else if (this.plugin.getAdapter().flags.isJoined) {
          this.adPosition = 'mid'
        }
        this.plugin.getAdapter().firePause()
        this.fireStart()
      }
    } catch (err) {
      youbora.Log.error(err)
    }
  },

  /** Listener for 'OnMediaError' event. */
  errorListener: function (e) {
    if (this.flags.isStarted || this.flags.isInited) {
      this.fireError(e.data.friendlyMessage || e.data.message)
      this.fireStop()
      this.plugin.getAdapter().fireResume()
    }
  },

  /** Listener for 'OnMediaEnd' event. */
  endListener: function (e) {
    this.fireStop()
    this.plugin.getAdapter().fireResume()
    this.resetValues()
  },

  /** Listener for 'OnMediaPlaying' event. */
  playingListener: function (e) {
    if (this.flags.isStarted || this.flags.isInited) {
      this.playhead = (e.data.currentTimeAggregate ? e.data.currentTimeAggregate : e.data.currentTime) / 1000
      this.fireJoin()
      if (this.monitor) this.monitor.skipNextTick()
    }
  },

  /** Listener for 'OnMediaPause' event. */
  pauseListener: function (e) {
    this.firePause()
  },

  /** Listener for 'OnMediaUnpause' event. */
  unpauseListener: function (e) {
    this.fireResume()
  },

  clickListener: function (e) {
    this.fireClick({ 'url': this.clickURL })
  },

  resetValues: function () {
    this.resource = null
    this.title = null
    this.duration = null
    this.playhead = null
    this.duration = null
    this.bitrate = null
    this.adPosition = null
    this.clickURL = null
  }
})

module.exports = AdsAdapter
