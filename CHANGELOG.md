## [6.7.1] - 2020-07-02
### Library
- Built with `6.7.11`

## [6.7.0] - 2020-05-29
### Library
- Built with `6.7.7`

## [6.5.2] - 2019-11-22
### Fixed
- Fake views with second error
### Library
- Built with `6.5.20`

## [6.5.1] - 2019-10-28
### Added
- Support to automatic integration
- New bitrate getter, and rendition too

## [6.5.0] - 2019-10-24
### Fixed
- Playhead and duration issues
### Added
- Small refactor
### Library
- Built with `6.5.18`

## [6.4.1] - 2019-06-04
### Fixed
- Monitor issue

## [6.4.0] - 2019-01-29
### Added
- Scope support
### Library
- Built with `6.4.13`

## [6.3.1] - 2018-09-03
### Fixed
- Adadapter not being created case

## [6.1.1] - 2018-02-14
### Added
- First release
